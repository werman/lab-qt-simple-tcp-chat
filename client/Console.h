#pragma once
#include <QObject>
#include <QSocketNotifier>
#include <QTextStream>

class Console : public QObject {
  Q_OBJECT
public:
  explicit Console(QObject *parent = 0);
  ~Console();

  void Run();
signals:
  void input(QString inputStr);

private slots:
  void ReadInput();

private:
  QSocketNotifier *m_notifier;

  inline QTextStream& qStdout()
  {
      static QTextStream r{stdout};
      return r;
  }
};
