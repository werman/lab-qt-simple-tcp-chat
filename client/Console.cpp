#include <iostream>
#include "Console.h"

Console::Console(QObject *parent) : QObject(parent) {
  m_notifier = new QSocketNotifier(fileno(stdin), QSocketNotifier::Read, this);
}

Console::~Console() {}

void Console::Run() {
  qStdout() << "Input: " << endl;
  qStdout() << "> " << flush;
  connect(m_notifier, SIGNAL(activated(int)), this, SLOT(ReadInput()));
}

void Console::ReadInput() {
  std::string line;
  std::getline(std::cin, line);
  qStdout() << "> " << flush;

  emit input(QString::fromStdString(line));
}
