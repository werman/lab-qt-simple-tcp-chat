#include "Console.h"
#include "Client.h"

Client::Client(QObject *parent) : QObject(parent) {
  m_console = new Console(this);
  connect(m_console, &Console::input, this, &Client::sendInput);
}

void Client::Connect() {
  m_socket = new QTcpSocket(this);
  m_socket->connectToHost("localhost", 11111);

  if (m_socket->waitForConnected(3000)) {
    qStdout() << "Connected!" << endl;
    connect(m_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
  } else {
    qStdout() << "Server didn't reply in 3 seconds!" << endl;
  }

  m_console->Run();
}

void Client::readyRead()
{
    auto bytes = m_socket->readAll();
    auto message = QString::fromUtf8(bytes);
    qStdout() << "> "<< message << endl;
}

void Client::sendInput(QString input)
{
    QByteArray buffer;
    buffer.append(input);
    m_socket->write(buffer);
    m_socket->flush();
}
