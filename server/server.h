#pragma once

#include <QTcpServer>

class ServerClientConnection;

class Server : public QTcpServer {
  Q_OBJECT

public:
  explicit Server(QObject *parent = 0);
  void StartServer();

protected:
  void incomingConnection(qintptr socketDescriptor) override;

private slots:
  bool onAuthAttempt(ServerClientConnection *connection, const QString name,
                     const QString password);
  void onMessageIncome(ServerClientConnection *connection,
                       const QString message);
  void onDisconnect(ServerClientConnection *connection);

private:
  static const QMap<QString, QString> k_users;

  QVector<ServerClientConnection *> m_connections;
};
