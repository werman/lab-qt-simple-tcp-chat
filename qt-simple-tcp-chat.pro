#-------------------------------------------------
#
# Project created by QtCreator 2015-12-12T18:55:37
#
#-------------------------------------------------

QT       += core
QT       += network

QT       -= gui

CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

server_config {
    TARGET = tcp-server
    DEFINES += "SERVER_APP"
}

client_config {
    TARGET = tcp-client
    DEFINES += "CLIENT_APP"
}

TEMPLATE = app


SOURCES += main.cpp \
    server/ServerClientConnection.cpp \
    server/Server.cpp \
    client/Client.cpp \
    client/Console.cpp

HEADERS += \
    server/ServerClientConnection.h \
    server/Server.h \
    client/Client.h \
    client/Console.h
