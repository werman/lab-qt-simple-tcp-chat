#include <QDebug>
#include "ServerClientConnection.h"
#include "Server.h"

const QMap<QString, QString> Server::k_users =
    QMap<QString, QString>({{"user1", "password"}, {"test", "test"}});

Server::Server(QObject *parent) : QTcpServer(parent) {}

void Server::StartServer() {
  if (listen(QHostAddress::Any, 11111)) {
    qDebug() << "Server: started";
  } else {
    qDebug() << "Server: not started!";
  }
}

void Server::incomingConnection(qintptr socketDescriptor) {
  auto client = new ServerClientConnection(this);
  client->SetSocket(socketDescriptor);

  connect(client, &ServerClientConnection::authAttempt, this,
          &Server::onAuthAttempt);
  connect(client, &ServerClientConnection::messageIncome, this,
          &Server::onMessageIncome);
  connect(client, &ServerClientConnection::disconnectClient, this,
          &Server::onDisconnect);

  m_connections.push_back(client);
}

bool Server::onAuthAttempt(ServerClientConnection *connection,
                           const QString name, const QString password) {
  const auto it = k_users.find(name);
  if (it != k_users.end()) {
    return (*it) == password;
  } else {
    return false;
  }
}

void Server::onMessageIncome(ServerClientConnection *connection,
                             const QString message) {
  for (auto c : m_connections) {
    if (c->IsAuthorized() && c != connection) {
      c->SendMessage(connection->GetName(), message);
    }
  }
}

void Server::onDisconnect(ServerClientConnection *connection) {
  m_connections.removeOne(connection);
}
