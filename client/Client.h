#pragma once

#include <QObject>
#include <QTcpSocket>

class Console;

class Client : public QObject {
    Q_OBJECT

public:
    explicit Client(QObject *parent = 0);
    void Connect();

private slots:
    void readyRead();
    void sendInput(QString input);

private:
    QTcpSocket *m_socket;
    Console *m_console;

    inline QTextStream& qStdout()
    {
        static QTextStream r{stdout};
        return r;
    }
};
