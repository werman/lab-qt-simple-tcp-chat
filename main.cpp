#include <QCoreApplication>

#ifdef SERVER_APP
#include "server/Server.h"
#endif

#ifdef CLIENT_APP
#include "client/Client.h"
#endif

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

#ifdef SERVER_APP
    qDebug() << "Server" << endl;
    Server server;
    server.StartServer();
#endif

#ifdef CLIENT_APP
    qDebug() << "Client" << endl;
    Client client;
    client.Connect();
#endif

    return a.exec();
}
