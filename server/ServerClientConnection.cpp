#include <QDebug>
#include <QThreadPool>
#include <QHostAddress>
#include <QString>
#include "ServerClientConnection.h"

ServerClientConnection::ServerClientConnection(QObject *parent)
    : QObject(parent) {}

ServerClientConnection::~ServerClientConnection() {}

void ServerClientConnection::SetSocket(qintptr descriptor) {
  m_socket = new QTcpSocket(this);

  qDebug() << "[ServerClientConnection] Creating connection";

  connect(m_socket, SIGNAL(connected()), this, SLOT(connected()));
  connect(m_socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
  connect(m_socket, SIGNAL(readyRead()), this, SLOT(readyRead()));

  m_socket->setSocketDescriptor(descriptor);
}

void ServerClientConnection::SendMessage(QString fromName, QString message) {
  SendStr(QString("[%1] %2").arg(fromName, message));
}

const QString &ServerClientConnection::GetName() const { return m_name; }

const bool ServerClientConnection::IsAuthorized() const {
  return m_isAuthorized;
}

void ServerClientConnection::connected() {
  qDebug() << "[ServerClientConnection] Client connected event";
}

void ServerClientConnection::disconnected() {
  qDebug() << "[ServerClientConnection] Client disconnected";
  emit disconnectClient(this);
}

void ServerClientConnection::readyRead() {
  auto bytes = m_socket->readAll();
  auto message = QString::fromUtf8(bytes);

  qDebug() << "[ServerClientConnection] Read: " << message;

  if (!m_isAuthorized) {
    qDebug() << "[ServerClientConnection] Authorize attempt";
    Authorize(message);
  } else {
    emit messageIncome(this, message);
  }
}

void ServerClientConnection::SendStr(const QString &message) {
  QByteArray buffer;
  buffer.append(message);
  m_socket->write(buffer);
}

void ServerClientConnection::Authorize(const QString &message) {
  auto parts = message.split("|");
  if (parts.size() != 2) {
    SendStr("Auth error");
    return;
  }

  bool authSuccess = emit authAttempt(this, parts[0], parts[1]);

  if (authSuccess) {
    m_name = parts[0];
    qDebug() << "[ServerClientConnection] Auth success.";
  } else {
    SendStr("Auth error");
    qDebug() << "[ServerClientConnection] Auth error.";
  }
}
