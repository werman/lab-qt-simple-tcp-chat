#pragma once

#include <QObject>
#include <QTcpSocket>

class QThreadPool;

class ServerClientConnection : public QObject {
  Q_OBJECT
public:
  explicit ServerClientConnection(QObject *parent);
  ~ServerClientConnection();

  void SetSocket(qintptr descriptor);
  void SendMessage(QString fromName, QString message);
  const QString &GetName() const;
  const bool IsAuthorized() const;
signals:
  bool authAttempt(ServerClientConnection *connection, const QString name,
                   const QString password);
  void messageIncome(ServerClientConnection *connection, const QString message);
  void disconnectClient(ServerClientConnection *connection);

public slots:

  void connected();
  void disconnected();
  void readyRead();

private:
  QThreadPool *m_threadPool;
  QTcpSocket *m_socket;

  QString m_name;
  bool m_isAuthorized;

  void SendStr(const QString &message);
  void Authorize(const QString &message);
};
